__author__ = 'zack.cassels'

from termcolor import cprint
import json, logging

logger = logging.getLogger(__name__)

class LogWrapper():
    def __init__(self, log, msg):
        self.log = log
        self.msg = msg

    def __enter__(self):
        self.log.info("START - " + self.msg)

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.log.info("FINISH - " + self.msg)

def log(response, *args, **kwargs):
    msg = " {} - {}\n".format(response.request.method, response.request.url)
    msg += "{} {} HTTP/1.1\n".format(response.request.method, response.request.url)
    msg += "HTTP/1.1 {} {}".format(response.status_code, response.reason)
    logger.debug(msg)

def getFormatedJSON(obj):
    return json.dumps(obj,
            indent=2,
            separators=(',', ': '),
            sort_keys=True)

def responseLog(response):
    msg = "{} - {}\n".format(response.status_code, response.reason)
    msg += "<--- Response ---<<<\n"
    msg += "HTTP/1.1 {} {}\n".format(response.status_code, response.reason)

    for option in response.headers:
        msg += "{}: {}\n".format(option, response.headers[option])

    if (len(response.text) > 0):
        if (response.text[0] == "{" or response.text[0] == "["):
            msg += getFormatedJSON(response.json())
        else:
            # msg += "(raw)"
            msg += response.text
    msg += "\n"
    logger.debug(msg)

def requestLog(request):
    msg = "{} - {}\n".format(request.method, request.url)
    msg += ">>>--- Request --->\n"
    msg += "{} {} HTTP/1.1\n".format(request.method, request.url)
    for option in request.headers:
        msg += "{}: {}\n".format(option, request.headers[option])

    if len(request.headers) > 0:
        msg = msg[:-1]

    msg += "\n"

    if not isinstance(request.body, str):
        msg += "{}".format(request.body)
        # msg += "(raw)"
    else:
        msg += "{}".format(getFormatedJSON(json.loads(request.body)))

    msg += "\n"
    logger.debug(msg)

def performanceLog(request):
    msg = "Elapsed time: {}".format(request.elapsed)
    # msg += "cookies: {}".format(request.cookies)
    logger.debug(msg)

def verboseLog(response, *args, **kwargs):
    requestLog(response.request)
    responseLog(response)
    performanceLog(response)
__author__ = 'zack.cassels'

import uuid
from urllib.parse import urlparse

from requests.auth import AuthBase

from requests import Session

from Common.HTTPResponseLogger import verboseLog, log


def addCorrelationId(request):
    correlation = "uploadTest_{}".format(uuid.uuid4())
    request.headers['ININ-Correlation-Id'] = correlation
    request.headers['x-inin-CorrelationId'] = correlation

class TokenBasedAuth(AuthBase):
    def __init__(self, session_id):
        self.session_id = session_id

    def __call__(self, r):
        r.headers['Authorization'] = "IPC {}".format(self.session_id)
        addCorrelationId(r)
        return r

class NoAuth(AuthBase):
    def __init__(self):
        pass

    def __call__(self, r):
        addCorrelationId(r)
        return r

class UserBasedAuth(AuthBase):
    def __init__(self, org, user_id):
        self.org = org
        self.user_id = user_id

    def __call__(self, r):
        r.headers['ININ-User-Id'] = self.user_id
        r.headers['ININ-Organization-Id'] = self.org
        addCorrelationId(r)
        return r

class PureCloud_Session(Session):
    def __init__(self, base_path, auth_method=None):
        super().__init__()
        self.headers = {
            "Accept": "*/*",
        }

        self.hooks = dict(response=verboseLog)
        # self.proxies = {"http": "localhost:8888", "https": "localhost:8888"}
        self.auth = auth_method
        self.base_path = base_path
        parse_result = urlparse(self.base_path)
        self.base_url = parse_result.scheme + "://" + parse_result.netloc
        self.org = None

    def create(self, user_id, password):
        raise NotImplementedError

    def __enter__(self):
        raise NotImplementedError

    def __exit__(self, exc_type, exc_val, exc_tb):
        raise NotImplementedError

    def addTokenAuth(self, session_id):
        if session_id:
            self.auth = TokenBasedAuth(session_id)
        else:
            self.auth = NoAuth()

    def get(self, path=None, full_url=None, **kwargs):
        """Sends a GET request. Returns :class:`Response` object.

        :param resource: URL for the new :class:`Request` object.
        :param \*\*kwargs: Optional arguments that ``request`` takes.
        """

        kwargs.setdefault('allow_redirects', True)

        if full_url is None:
            url = self.base_path + path
        else:
            url = full_url

        return self.request('GET', url, **kwargs)

    def options(self, path, **kwargs):
        """Sends a OPTIONS request. Returns :class:`Response` object.

        :param url: URL for the new :class:`Request` object.
        :param \*\*kwargs: Optional arguments that ``request`` takes.
        """

        kwargs.setdefault('allow_redirects', True)
        url = self.base_path + path

        return self.request('OPTIONS', url, **kwargs)

    def head(self, path, **kwargs):
        """Sends a HEAD request. Returns :class:`Response` object.

        :param url: URL for the new :class:`Request` object.
        :param \*\*kwargs: Optional arguments that ``request`` takes.
        """

        kwargs.setdefault('allow_redirects', False)
        url = self.base_path + path

        return self.request('HEAD', url, **kwargs)

    def post(self, path, data=None, json=None, **kwargs):
        """Sends a POST request. Returns :class:`Response` object.

        :param url: URL for the new :class:`Request` object.
        :param data: (optional) Dictionary, bytes, or file-like object to send in the body of the :class:`Request`.
        :param json: (optional) json to send in the body of the :class:`Request`.
        :param \*\*kwargs: Optional arguments that ``request`` takes.
        """
        url = self.base_path + path

        return self.request('POST', url, data=data, json=json, **kwargs)

    def put(self, path, data=None, **kwargs):
        """Sends a PUT request. Returns :class:`Response` object.

        :param url: URL for the new :class:`Request` object.
        :param data: (optional) Dictionary, bytes, or file-like object to send in the body of the :class:`Request`.
        :param \*\*kwargs: Optional arguments that ``request`` takes.
        """
        url = self.base_path + path

        return self.request('PUT', url, data=data, **kwargs)

    def patch(self, path, data=None, **kwargs):
        """Sends a PATCH request. Returns :class:`Response` object.

        :param url: URL for the new :class:`Request` object.
        :param data: (optional) Dictionary, bytes, or file-like object to send in the body of the :class:`Request`.
        :param \*\*kwargs: Optional arguments that ``request`` takes.
        """
        url = self.base_path + path

        return self.request('PATCH', url,  data=data, **kwargs)

    def delete(self, path, **kwargs):
        """Sends a DELETE request. Returns :class:`Response` object.

        :param url: URL for the new :class:`Request` object.
        :param \*\*kwargs: Optional arguments that ``request`` takes.
        """
        url = self.base_path + path

        return self.request('DELETE', url, **kwargs)

    def health_check(self, **kwargs):
        url = self.base_url + "/health/check"
        return self.request("GET", url, **kwargs)

class Public_API(PureCloud_Session):
    def __init__(self, region, host, session_id=None):
        url = "https://public-api.{}.{}".format(region, host)
        super().__init__(url)
        self.user_Id = None
        self.addTokenAuth(session_id)

    def create(self, user_id, password):
        session = {"userIdentifier": user_id, "password": password}
        session_info = self.post("/api/v1/auth/sessions", json=session).json()
        self.auth = TokenBasedAuth(session_info["id"])
        self.user_Id = session_info["userId"]
        return self

    def close(self):
        if isinstance(self.auth, TokenBasedAuth):
            self.delete("/api/v1/auth/sessions/{}".format(self.auth.session_id))

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.close()

class Donut_Service(PureCloud_Session):
    def __init__(self, region, host, session_id=None):
        url = "http://donut.{}.{}".format(region, host)
        super().__init__(url)
        self.addTokenAuth(session_id)

class Upload_Service(PureCloud_Session):
    def __init__(self, region, host, session_id=None):
        url = "https://upload.{}.{}/uploads".format(region, host)
        super().__init__(url)
        self.auth = NoAuth()
        # self.addTokenAuth(session_id)

class Content_Management(PureCloud_Session):
    def __init__(self, region, host, session_id=None):
        url = "http://content-management.{}.{}/content_management".format(region, host)
        super().__init__(url)
        self.addTokenAuth(session_id)

class Configuration_Service(PureCloud_Session):
    def __init__(self, region, host, session_id=None):
        url = "http://configuration.{}.{}/configurations".format(region, host)
        super().__init__(url)
        self.addTokenAuth(session_id)

class Audit_Service(PureCloud_Session):
    def __init__(self, region, host, org, user_id):
        url = "http://audit.{}.{}/".format(region, host)
        super().__init__(url)
        self.auth = UserBasedAuth(org, user_id)

class Directory_Service(PureCloud_Session):
    def __init__(self, host, session_id=None):
        url = "http://directory.{}".format(host)
        super().__init__(url)
        self.addTokenAuth(session_id)

class UI_API_Service(PureCloud_Session):
    def __init__(self, region, host):
        url = "http://apps.{}.{}/".format(region, host)
        super().__init__(url)
        self.auth = NoAuth()

class Donut_Events_Service(PureCloud_Session):
    def __init__(self, region, host):
        url = "http://donut-events.{}.{}/"
        super().__init__(url)
        self.auth = NoAuth()

## PUT USER SCRIPTS HERE ##

def print_named_permissions(user_name, password):
    with Public_API(host).create(user_name, password) as public_api:
        # adminSession.get("/api/v1/configuration/roles/e39239a9-7158-4e16-aa45-c79bb4e2a7d2")
        users = public_api.get("/api/v1/users?pageSize=100&pageNumber=1&username={}".format(user_name)).json()

        permissions = public_api.get("/api/v1/authorization/permissions").json()
        permissions_map = {}

        for permission in permissions["entities"]:
            permissions_map[permission["domain"]] = permission

        for user in users["entities"]:
            # user_details = adminSession.get("/api/v1/users/{}".format(user["id"])).json()
            org_roles = public_api.get("/api/v1/authorization/users/{}/roles".format(user["id"])).json()["roles"]
            roles = public_api.get("/api/v1/configuration/roles?pageSize=500&pageNumber=1").json()
            roles_indexed = {}
            matched_roles = []

            public_api.get("/api/v1/sessions/{}".format(public_api.auth.session_id))

            for role in roles["entities"]:
                roles_indexed[role["id"]] = role

            print("Org defined roles")
            for org_role in org_roles:
                if (org_role in roles_indexed):
                    # print("match id -> ", roles_indexed[admin_roles["id"]]["id"])
                    print(" ", roles_indexed[org_role]["name"])
                    # adminSession.get("/api/v1/authorization/roles/{}".format(roles_indexed[org_role]["id"]))
                    matched_roles.append(roles_indexed[org_role])
                else:
                    matched_roles.append(org_role + "(Deleted)")

if __name__ == "__main__":

     print_named_permissions("zcassels@example.com", "test1234")


     host = "inindca.com"
     # host = "ininsca.com"
     # host = "inintca.com"

     # user = "autoadmin211b.donotdelete.jciwr@example.com"
     user = "zcassels@example.com"
     # user = "AutoPerson.qgkizx1428332340107xtcValidateTextExtractionAndThumbnailCreationHappenedToDOCFile_NYbpBusf_4d74dd2e5ef5@example.com"
     # user = "AutoPerson.rtzhpx1428345339159xtcValidateTextExtractionAndThumbnailCreationHappenedToDOCFile_JBucyElH_234250cc3ab8@example.com"
     # user = "AutoPerson.aipbxx1428346528965xtcValidateTextExtractionAndThumbnailCreationHappenedToDOCFile_QKkjXzLv_06beb7500693@example.com"
     # user = "CMTestAdmin_SCA@example.com"


     # doc = "dba6aa2f-0824-4284-9be1-ed86dbee8edf"
     # doc = "7a45c9ee-6d0b-4e7b-840c-eb9f94fc8409"
     doc = "58edb248-0983-45a3-9228-31122f4e6449"

     # org = "211b589c-6907-40d2-909c-6eaaea3c8985"
     org = "b5316aba-fffb-4fcb-9a2f-41269ba89d2c"

     password = "test1234"

     with Public_API(host).create(user, password) as public_api:

        # session_id = public_api.auth.session_id
        session_id = "5ivtOpvl7nwElZ478g66N4"

        donut = Donut_Service(host, session_id)
        cm = Content_Management(host, session_id)
        upload = Upload_Service(host, session_id)
        config = Configuration_Service(host, session_id)
        audit = Audit_Service(host, org, public_api.user_Id)
        apps = UI_API_Service(host)

        roles_response = public_api.get("/api/v1/configuration/roles?pageSize=500&pageNumber=1").json()
        roles = roles_response["entities"]

        # user = "015d5331-75b5-4ff5-be0c-edc90b94530d"
        # user = "7eb7190c-9c07-49a2-ae34-b231150caa47"

        # public_api.get("/api/v1/users?pageSize=100&pageNumber=1&username={}".format(user))

        # public_api.get("/api/v1/authorization/permissions")
        # public_api.get("/api/v1/authorization/users/{}/roles".format(user))
        # donut.get("/organizations/{}/users/{}".format(org, user))


        # document = {"name":"foobar.text"}

        # response = public_api.post("/api/v1/contentmanagement/documents".format(doc), json=document)
        # doc_id = response.json()["id"]
        # public_api.get("/api/v1/documents/{}".format(doc_id))

        # donut.get("/health/check")
        # cm.get("/health/check")
        # upload.get("/health/check")
        # config.get("/health/check")
        # audit.get("/health/check")
        # apps.get("/health/check")

        #
        # auditQueryStr = {
        #     "levelFilter": "USER,SYSTEM"
        # }
        #
        # entities = []
        #
        # audits = public_api.get("/api/v1/contentmanagement/documents/{}/audits".format(doc), params=auditQueryStr).json()
        # entities.append(audits["entities"])
        #
        # for entity in audits["entities"]:
        #     auditQueryStr["transactionFilter"] = entity["transactionId"]
        #     entities.append(public_api.get("/api/v1/contentmanagement/documents/{}/audits".format(doc), params=auditQueryStr))
        #
        # auditQuery = {
        #     "organizationId": org,
        #     "filters": [
        #         {"name": "entity.id", "type": "STRING", "operator": "EQUALS", "values": [doc]},
        #         # {"name": "transactionId", "type": "STRING", "operator": "EQUALS", "values": [trans_id]},
        #         {"name": "entity.type", "type": "STRING", "operator": "EQUALS", "values": ["DOCUMENT"]}
        #     ]
        # }
        #
        # audit.post("/audit/v1/audits/query", json=auditQuery)


__author__ = 'zack.cassels'

import yaml, uuid, random
from PureCloud import *

from enum import Enum
import logging, re

logger = logging.getLogger(__name__)

class Environment(Enum):
    DEVELOPMENT = "DEV"
    TESTING = "TEST"
    STAGE = "STAGE"
    PRODUCTION = "PROD"
    LOCAL = "LOCAL"
    TEST_LOCAL = "TEST_LOCAL"
    PRODUCTION_AUSTRALIA = "PROD_ANZ"

class EnvironmentSettings():
    server = None
    admin_user = None
    admin_password = None
    organization = None
    region = None

class Client():
    def __init__(self, server, region, org):
        self.user_name = None
        self.user_id = None

        self.org = org

        self.server = server
        self.session_id = None

        self.public_api = Public_API(region, server, self.session_id)
        self.donut = Donut_Service(region, server, self.session_id)
        self.cm = Content_Management(region, server, self.session_id)
        self.upload = Upload_Service(region, server, self.session_id)
        self.config = Configuration_Service(region, server, self.session_id)
        self.audit = Audit_Service(region, server, org, self.session_id)
        self.directory = Directory_Service(server, self.session_id)
        self.apps = UI_API_Service(region, server)

    def establish(self, user, password):
        self.password = password
        self.user_name = user

        self.public_api = self.public_api.create(user, password)
        self.user_id = self.public_api.user_Id
        self.session_id = self.public_api.auth.session_id

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.close()

    def get_document(self):
        self.public_api.post("/api/document")

    def close(self):
        self.public_api.close()


class Test_Client():
    def __init__(self, env_name):
        with open("../app.config", "r") as config_file:
            conf = yaml.safe_load(config_file)

        running_config = EnvironmentSettings()

        for environment in conf["ENVIRONMENTS"]:
            if environment["name"] == env_name.value:
                logger.debug("Using environment -> " + env_name.value)
                running_config.server = environment["DOMAIN"]
                # running_config.admin_password = environment["PCADMIN_PASSWORD"]
                # running_config.admin_user = environment["PCADMIN_EMAIL"]
                # running_config.organization = environment["ORG_ID"]
                running_config.admin_user = environment["CMADMIN_EMAIL"]
                running_config.admin_password = environment["CMADMIN_PASSWORD"]
                running_config.organization = environment["CM_ORG_ID"]

                public_api_url = environment["PUBLIC_API_URL"]
                result = re.match(r"^https:\/\/(?P<service>[a-zA-Z0-9-_]*?)\.(?P<region>[a-zA-Z0-9-_]*?)\.(?P<domain>[a-zA-Z0-9-_.]*)", public_api_url)
                running_config.region = result.group("region")

                logger.debug("Settings [server={}, user={}, password={}, org={}]".format(running_config.server, running_config.admin_user, running_config.admin_password, running_config.organization))

        self.config = running_config
        self.admin = Client(self.config.server, self.config.region, self.config.organization)

        if running_config.admin_user is None:
            logger.info("No PCAdmin credentials")
        else:
            self.admin.establish(self.config.admin_user, self.config.admin_password)

        self.registered_users = []
        self.registered_sessions = []


    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.close()

    def get_user(self):
        roles = self.admin.public_api.get("/api/v1/configuration/roles?pageSize=500&pageNumber=1").json()
        roles_indexed = {}
        matched_roles = []

        for role in roles["entities"]:
            roles_indexed[role["name"]] = role

        # print(roles_indexed["contentManagementAdmin"])
        # print(roles_indexed["contentManagementUser"])
        # print(roles_indexed["employee"])

        user_contract = {
            "name":"zcassels_test_user",
             "username":"zcassels_test_user",
            "email":"zcassels_test_user_{}@example.com".format(uuid.uuid4()),
            # "displayName":"zcassels_test_user",
            "phoneNumber":"3175551234",
            "department":"Department",
            "title":"MyTitle",
            "password":"test1234"
            # "userImages":[]
        }

        response = self.admin.public_api.post("/api/v1/users/", json=user_contract)
        self.registered_users.append(response.json())

        add_roles = [roles_indexed["contentManagementUser"]["id"], roles_indexed["contentManagementAdmin"]["id"], roles_indexed["employee"]["id"]]
        self.admin.public_api.put("/api/v1/authorization/users/{}/roles".format(response.json()["id"]), json=add_roles)
        return response.json()

    def get_session(self):
        user_info = self.get_user()
        new_session = Client(self.config.server, self.config.region, self.config.organization)
        new_session.establish(user_info["email"], "test1234")
        self.registered_sessions.append(new_session)
        return new_session

    def close(self):
        for session in self.registered_sessions:
            session.close()

        for user in self.registered_users:
            self.admin.config.delete("/v1/organizations/{}/users/{}".format(self.config.organization, user["id"]))

        self.admin.public_api.close()

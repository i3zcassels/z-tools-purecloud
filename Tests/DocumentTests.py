__author__ = 'zack.cassels'

import uuid, sys, time

import Tests.CMTests
from Tests.CMTests import ContentManagementBaseTest
from Common.HTTPResponseLogger import log, verboseLog, LogWrapper
import logging
logging.getLogger("requests").setLevel(logging.WARNING)
logger = logging.getLogger(__name__)
import requests

class Retry:
    @staticmethod
    def until_true(action, timeout, interval):
        timeout = time.time() + timeout
        while time.time() < timeout:
            result = action()
            if result:
                return
            time.sleep(interval)

class DocumentTest(ContentManagementBaseTest):
    def setUp(self):
        with LogWrapper(logger, "SETUP " +  sys._getframe().f_code.co_name):
            super().setUp()

            # Print user info
            self.create_workspace()

            logger.info("Test User -> {}".format(self.testUser.user_name))
            logger.info("Workspace -> {}".format(self.ws["name"]))



    def create_workspace(self):
        workspace = {"type": "GROUP", "name": "zcassels_test_workspace_{}".format(uuid.uuid4())}
        create_ws_response = self.testUser.public_api.post("/api/v1/contentmanagement/workspaces/", json=workspace)
        self.ws = create_ws_response.json()

        def poll_ws():
            return self.testUser.public_api.get("/api/v1/contentmanagement/workspaces/" + self.ws["id"]).status_code == 200

        Retry.until_true(poll_ws, .25, 1)

        workspace_member = {"memberType": "USER"}
        self.testUser.public_api.put("/api/v1/contentmanagement/workspaces/{}/members/{}".format(self.ws["id"], self.testUser.user_id), json=workspace_member)


    def create_document(self, file):
        create_document = {"name": "zcassels_test_document_{}".format(uuid.uuid4()), "systemType": "DOCUMENT", "workspace": {"id": self.ws["id"]}}

        create_document_response = self.testUser.public_api.post("/api/v1/contentmanagement/documents/", json=create_document)
        document = create_document_response.json()

        # document_response = self.testUser.public_api.get("/api/v1/contentmanagement/documents/{}".format(document["id"]))
        dest_url = document["uploadDestinationUri"]
        tokens = dest_url.split("/")
        url = tokens[len(tokens)-1]
        # files = {'file': ('report.xls', open('CM_Text.txt', 'rb'), "text/plain")}

        self.testUser.upload.post("/cm/{}".format(url), files=file)

        timeout = time.time() + 60*5
        found = False
        while time.time() < timeout:
            doc_status = self.testUser.public_api.get("/api/v1/contentmanagement/status/{}".format(document["id"]))
            if doc_status.json()["statusCode"] == "COMPLETE":
                found = True
                logger.info("Polling for COMPLETE status successful")
                break
            time.sleep(.25)

        document = self.testUser.public_api.get("/api/v1/contentmanagement/documents/{}".format(document["id"])).json()
        return document


    def tearDown(self):
        with LogWrapper(logger, "TEAR DOWN"):
            if self.ws is not None:
                self.testUser.public_api.delete("/api/v1/contentmanagement/workspaces/{}".format(self.ws["id"]))
            super().tearDown()

    def test_no_test_body(self):
        with LogWrapper(logger, "TEST " + sys._getframe().f_code.co_name):
            pass

    def test_workspace_search(self):
        with LogWrapper(logger, "TEST " + sys._getframe().f_code.co_name):
            query_params = {"workspaceId": self.ws["id"]}
            self.testUser.public_api.get("/api/v1/contentmanagement/documents/", params=query_params)

            query= {"pageNumber": 1,
                    "pageSize": 25,
                    "queryPhrase": "",
                    "facetNameRequests": ["tags", "createdByDisplayName", "contentType", "name", "contentLength", "dateCreated"],
                    "sort": [{"name":"name","ascending": False}],
                    "filters": [{"systemFilter": False, "id": "workspaceId", "type":"STRING", "name": "workspaceId", "operator":"EQUALS", "values": [self.ws["id"]]}]}
            self.testUser.public_api.post("/api/v1/contentmanagement/query", json=query)


    def test_get_orgs(self):
        with LogWrapper(logger, "TEST " + sys._getframe().f_code.co_name):
            # b5316aba-fffb-4fcb-9a2f-41269ba89d2c
            # self.testUser.directory.get("/health/check/")
            self.testUser.directory.get("/api/v2/orgs/{}".format(self.org))

    def get_audits(self):
        audit_params = {"level": "SYSTEM,USER"}
        audit_Response = self.testUser.public_api.get("/api/v1/contentmanagement/documents/{}/audits".format(self.document["id"]))
        audits = audit_Response.json()
        actions = []
        for entity in audits["entities"]:
            audit_params["transactionFilter"] = entity["transactionId"]
            entitity_response = self.testUser.public_api.get("/api/v1/contentmanagement/documents/{}/audits".format(self.document["id"]), params=audit_params).json()
            entities = entitity_response["entities"]
            for action in entities:
                actions.append(action["action"])


    def test_TES_audits(self):
        with LogWrapper(logger, "TEST " + sys._getframe().f_code.co_name):

            file = {'file': ('report.xls', open('CM_docx.docx', 'rb'), "application/msword")}
            self.document = self.create_document(self.file)

            # Get content
            queryParams = {"queryPhrase": "Time To Think", "pageNumber": 1, "pageSize": 5, "sort": [], "filters": []}
            self.testUser.public_api.post("/api/v1/contentmanagement/query", json=queryParams)

            self.get_audits()

    def test_Thumbnail_audits(self):
        with LogWrapper(logger, "TEST " + sys._getframe().f_code.co_name):
            self.file = {'file': ('report.xls', open('CM_png.png', 'rb'), "image/png")}
            self.document = self.create_document(self.file)

            # Get thumbnail
            self.testUser.public_api.get("/api/v1/contentmanagement/documents/{}".format(self.document["id"]))
            self.get_audits()

    def test_xml_audit(self):
        with LogWrapper(logger, "TEST " + sys._getframe().f_code.co_name):
            self.file = {'file': ('XML', open('./Resources/CM_xml', 'rb'), "text/xml")}
            self.document = self.create_document(self.file)
            content = self.testUser.public_api.get("/api/v1/contentmanagement/documents/{}/content".format(self.document["id"])).json()
            self.testUser.public_api.get(full_url=content["contentLocationUri"])

            time.sleep(10)

            # Get thumbnail
            self.get_audits()

    def test_remove_role(self):
        with LogWrapper(logger, "TEST " + sys._getframe().f_code.co_name):
            self.file = {'file': ('XML', open('./Resources/CM_xml', 'rb'), "text/xml")}
            self.document = self.create_document(self.file)
            content = self.testUser.public_api.get("/api/v1/contentmanagement/documents/{}/content".format(self.document["id"])).json()
            self.testUser.public_api.get(full_url=content["contentLocationUri"])

            query = {
                "workspace": self.ws["id"],
                "name": self.document["id"]
            }
            self.testUser.public_api.get("/api/v1/contentmanagement/documents/", params=query)

    def test_html_audit(self):
        with LogWrapper(logger, "TEST " + sys._getframe().f_code.co_name):
            self.file = {'file': ('HTML_FILE', open('./Resources/CM_html.html', 'rb'), "text/html")}
            self.document = self.create_document(self.file)

            time.sleep(10)

            # Get thumbnail
            self.get_audits()


    def test_tag_search(self):
        pass


    def test_create_share(self):
        self.file = {'file': ('HTML_FILE', open('./Resources/CM_html.html', 'rb'), "text/html")}
        self.document = self.create_document(self.file)

        # -- ShareRequest
        # private String entityId;
        # private SharedEntityType entityType;
        # private String sharedToMemberId;
        # private MemberType sharedToMemberType;
        # private List<ShareMember> sharedToMembers;

        # -- ShareMember
        # private String id;
        # private String name;
        # private MemberType type;

        create_share = {
            "entityId": self.document["id"],
            "entityType": "DOCUMENT",
            "sharedToMembers": [{"id": self.test_client.admin.user_id, "type": "USER"}]
        }

        self.testUser.public_api.post("/api/v1/contentmanagement/shares/", json=create_share)

    def test_shares(self):
        self.document = self.create_document(self.file)
        # Second User ?
        self.second_user = self.test_client.get_session()
        self.testUser.public_api.put("/api/v1/contentmanagement/workspaces/{}/members/{}".format(self.ws["id"], self.second_user.user_id), json={"memberType": "USER"})

        tokens = self.document["downloadSharingUri"].split("/")
        shared_id = tokens[len(tokens)-1]
        with LogWrapper(logger, "TEST " + sys._getframe().f_code.co_name):

            params = {
                "disposition": "inline"
            }

            response = self.second_user.public_api.get("/api/v1/contentmanagement/shared/{}".format(shared_id), params=params)

            # follow location header
            # requests.get(response.headers["location"], hooks=dict(response=verboseLog))
__author__ = 'zack.cassels'

import Tests.PureCloudTests, sys

import logging, time
logging.getLogger("requests").setLevel(logging.WARNING)
logger = logging.getLogger(__name__)

from Tests.PureCloudTests import PureCloudTests
from Common.HTTPResponseLogger import LogWrapper

class ContentManagementBaseTest(PureCloudTests):
    def setUp(self):
        super().setUp()
        self.testUser = self.test_client.get_session()

    def tearDown(self):
        super().tearDown()

    def test_get_org(self):
        with LogWrapper(logger, "TEST " +  sys._getframe().f_code.co_name):
            self.testUser.public_api.get("/api/v1/configuration/organization")

    def test_get_self(self):
        with LogWrapper(logger, "TEST " +  sys._getframe().f_code.co_name):
            # self.testUser.public_api.get("/api/v1/contentmanagement/documents?workspaceId={}&name={}".format(self.testUser.))
            pass

    def test_donut_service(self):
        with LogWrapper(logger, "TEST " +  sys._getframe().f_code.co_name):

            user_id = self.testUser.user_id


            # user_id = "93fdd58d-19dc-4ee2-99ac-25baeeeb4cfc"
            #

            # self.testUser.donut.get("/organizations/{}/roles".format(self.org, user_id))
            # self.testUser.donut.get("/organizations/{}/users/{}/roles".format(self.org, user_id))
            # self.testUser.public_api.get("/api/v1/authorization/users/{}/roles".format(user_id))

            # self.testUser.donut.get("/organizations/{}/users/{}/roles".format(self.org, user_id))
            # self.testUser.public_api.get("/api/v1/authorization/users/{}/roles".format(user_id))
            # self.testUser.public_api.get("/api/v1/authorization/users/{}/permissions".format(user_id))
            # self.testUser.donut.get("/organizations/{}/users/{}/roles".format(self.org, user_id))

            # self.testUser.donut.get("/organizations/{}/users/{}".format(self.org, user_id))
            # self.testUser.donut.get("/v1/organizations/{}/users/{}".format(self.org, user_id))
            # self.testUser.donut.get("/v1/organizations/{}/users/{}/policies".format(self.org, user_id))

            time.sleep(60)
            self.testUser.donut.get("/organizations/{}/users/{}".format(self.org, user_id))
            self.testUser.donut.get("/organizations/{}/users/{}/roles".format(self.org, user_id))
            self.testUser.donut.get("/organizations/{}/users/{}/permissions".format(self.org, user_id))
            # self.testUser.public_api.get("/api/v1/authorization/users/{}/roles".format(user_id))

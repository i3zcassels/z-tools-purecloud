__author__ = 'zack.cassels'

import unittest, logging.config, logging, uuid, yaml

logging.config.fileConfig('logging.ini')
logging.getLogger("requests").setLevel(logging.WARNING)

from TestClient import Test_Client, Environment

logger = logging.getLogger(__name__)

class InfoFilter(logging.Filter):
    def filter(self, rec):
        return False

class InfoHandler(logging.StreamHandler):
    def __init__(self, *args, **kwargs):
        logging.StreamHandler.__init__(self, *args, **kwargs)
        self.addFilter(InfoFilter())

logger.addFilter(InfoFilter())

from Common.HTTPResponseLogger import log

class PureCloudTests(unittest.TestCase):
    def setUp(self):
        env = Environment.DEVELOPMENT
        self.use_cache = True
        self.test_client = Test_Client(env)
        self.org = self.test_client.config.organization

    def tearDown(self):
        self.test_client.close()

    def test_feature_flag(self):
        self.test_client.admin.public_api.get("/api/v1/featuretoggles")

    def test_health_checks(self):
        # health_info = self.test_client.admin.public_api.get("/health/check")
        # self.test_client.admin.donut.get("/health/check")
        # self.test_client.admin.audit.get("/health/check")
        self.test_client.admin.config.get("/health/check")
        # self.test_client.admin.cm.get("/health/check")

    def test_create_org(self):
        organization_model = {
            "adminUsername": "Admin_Prod_ANZ@example.com",
            "adminPassword": "#!TEST1NG!#",
            "name": "Admin_Prod_ANZ",
            "thirdPartyOrgName": "Admin_Prod_ANZ"
        }
        self.test_client.admin.config.post("/v2/organizations", json=organization_model)